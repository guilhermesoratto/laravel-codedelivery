@extends('app')
@section('content')

    <div class="container">
        <legend>Editando produto: {{$product->name}}</legend>

        @include('errors._check')

        {!! Form::model($product, ['route' =>[ 'admin.products.update', $product->id]]) !!}

        @include('admin.products._form')

        {!! Form::close() !!}

    </div>




@endsection
