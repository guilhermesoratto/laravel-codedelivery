@extends('app')
@section('content')

    <div class="container">

        <div class="page-header">
            <h3>Produtos</h3>
        </div>

        <a href="{{ route('admin.products.create') }}" class="btn btn-success glyphicon glyphicon-plus" title="Adicionar"></a>
        <br><br>
        <table class="table table-striped table-condensed">
            <thead>
            <tr>
                <th>Id</th>
                <th>Produto</th>
                <th>Categoria</th>
                <th>Preço</th>
                <th>Ação</th>
            </tr>
            </thead>

            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->id }}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->category->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>
                        <a href="{{route('admin.products.edit' , ['id' => $product->id])}}"
                           class="btn btn-inverse glyphicon glyphicon-pencil " title="Editar"></a>
                        <a href="{{route('admin.products.destroy' , ['id' => $product->id])}}"
                           class="btn btn-inverse glyphicon glyphicon-remove " title="Remover"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $products->render() !!}

    </div>




@endsection
