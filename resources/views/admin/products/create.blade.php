@extends('app')
@section('content')

    <div class="container">
        <legend>Novo produto</legend>

        @include('errors._check')

        {!! Form::open(['route' => 'admin.products.store'], ['class' => 'form']) !!}

        @include('admin.products._form')

        {!! Form::close() !!}

    </div>




@endsection
