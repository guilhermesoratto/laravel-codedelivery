@extends('app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h3>Categorias</h3>
        </div>

        <a href="{{ route('admin.categories.create') }}" class="btn btn-success glyphicon glyphicon-plus" title="Adicionar"></a>
        <br><br>
        <table class="table table-striped table-condensed">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Ação</th>
            </tr>
            </thead>

            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{$category->name}}</td>
                    <td>
                        <a href="{{route('admin.categories.edit' , ['id' => $category->id])}}"
                           class="btn btn-inverse glyphicon glyphicon-pencil " title="Editar"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $categories->render() !!}

    </div>




@endsection
