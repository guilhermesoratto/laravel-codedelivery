@extends('app')
@section('content')

    <div class="container">
        <legend>Nova categoria</legend>

        @include('errors._check')

        {!! Form::open(['route' => 'admin.categories.store'], ['class' => 'form']) !!}

        @include('admin.categories._form')

        {!! Form::close() !!}

    </div>




@endsection
