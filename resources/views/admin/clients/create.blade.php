@extends('app')
@section('content')

    <div class="container">
        <legend>Novo Cliente</legend>

        @include('errors._check')

        {!! Form::open(['route' => 'admin.clients.store'], ['class' => 'form']) !!}

        @include('admin.clients._form')

        {!! Form::close() !!}

    </div>




@endsection
