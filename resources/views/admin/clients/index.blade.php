@extends('app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h3>Clientes</h3>
        </div>

        <a href="{{ route('admin.clients.create') }}" class="btn btn-success glyphicon glyphicon-plus" title="Adicionar"></a>
        <br><br>
        <table class="table table-striped table-condensed">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Ação</th>
            </tr>
            </thead>

            <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{$client->id }}</td>
                    <td>{{$client->user->name}}</td>
                    <td>
                        <a href="{{route('admin.clients.edit' , ['id' => $client->id])}}"
                           class="btn btn-inverse glyphicon glyphicon-pencil " title="Editar"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $clients->render() !!}

    </div>




@endsection
