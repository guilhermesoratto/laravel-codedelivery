@extends('app')
@section('content')

    <div class="container">
        <legend>Editando cliente: {{$client->user->name}}</legend>

        @include('errors._check')

        {!! Form::model($client, ['route' =>[ 'admin.clients.update', $client->id]]) !!}

        @include('admin.clients._form')

        {!! Form::close() !!}

    </div>




@endsection
