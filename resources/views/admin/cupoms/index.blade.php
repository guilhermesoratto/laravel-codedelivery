@extends('app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h3>Categorias</h3>
        </div>

        <a href="{{ route('admin.cupoms.create') }}" class="btn btn-success glyphicon glyphicon-plus" title="Adicionar"></a>
        <br><br>
        <table class="table table-striped table-condensed">
            <thead>
            <tr>
                <th>ID</th>
                <th>Código</th>
                <th>Valor</th>
                <th>Ação</th>
            </tr>
            </thead>

            <tbody>
            @foreach($cupoms as $cupom)
                <tr>
                    <td>{{ $cupom->id }}</td>
                    <td>{{ $cupom->code}}</td>
                    <td>{{ $cupom->value}}</td>
                    <td>
                        <a href=""
                           class="btn btn-inverse glyphicon glyphicon-pencil " title="Editar"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $cupoms->render() !!}

    </div>




@endsection
