@extends('app')
@section('content')

    <div class="container">
        <legend>Novo Cupom</legend>

        @include('errors._check')

        {!! Form::open(['route' => 'admin.cupoms.store'], ['class' => 'form']) !!}

        @include('admin.cupoms._form')

        {!! Form::close() !!}

    </div>




@endsection
