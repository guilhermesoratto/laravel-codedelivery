@extends('app')
@section('content')

    <div class="container">
        <legend>Editando categoria: {{$category->name}}</legend>

        @include('errors._check')

        {!! Form::model($category, ['route' =>[ 'admin.categories.update', $category->id]]) !!}

        @include('admin.categories._form')

        {!! Form::close() !!}

    </div>




@endsection
